#Import data for covid-19 cases and AF bases
import pandas as pd
import requests
import io
import geopy.distance
import scipy as sp
import scipy.spatial
import scipy.optimize

import us
import numpy as np

#Data is under the creative commons licence.
#https://usafacts.org/visualizations/coronavirus-covid-19-spread-map/
COVIDByCounty_url = 'https://static.usafacts.org/public/data/covid-19/covid_confirmed_usafacts.csv'
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
response = requests.get(url=COVIDByCounty_url,headers=headers)

COVID_Data = pd.read_csv(io.StringIO(response.text[response.text.find('countyFIPS',0):]))
response.close()
As_of_date = COVID_Data.columns[-1]

County_Info = pd.read_csv('County_Info.csv')

AFB_Locs = pd.read_csv('AFB_Locs.csv')

COVID_Data = pd.merge(County_Info,
                      COVID_Data,
                      right_on='countyFIPS',
                      left_on='FIPS',
                      how='inner')

#Calculate distances from all conuties to all bases
#This could be sped up if needed
distances = pd.DataFrame(sp.spatial.distance.cdist(COVID_Data.loc[:,['Latitude','Longitude']],AFB_Locs.loc[:,['Lat','Long']],lambda u, v: geopy.distance.distance(u,v).miles))

#Flag counties that are within 100 miles of the AFB
#This is based on the coords of the center of the county from wikipedia
Nearby_Counties = distances < 50

#Generate statistics for each AFB based on nearby counties
AFB_Locs['Nearby Pop'] = pd.DataFrame(Nearby_Counties.T * COVID_Data.loc[:,'Population']).T.sum(axis=0)
AFB_Locs['Case List']=[]
AFB_Locs['Rate List']=[]
Case_Range = range(-15,0)
for day_offset in Case_Range:
    AFB_Locs.insert(len(AFB_Locs.columns),str(day_offset) + ' days cases', (Nearby_Counties.T * COVID_Data.iloc[:, day_offset]).T.sum(axis=0),True)
    AFB_Locs[str(day_offset) + ' days rate'] = AFB_Locs[str(day_offset) + ' days cases']/(AFB_Locs['Nearby Pop']/1000)
    AFB_Locs['Case List'].append

AFB_Locs['Projected Cases 5'] = 0
AFB_Locs['Projected Cases 10'] = 0
AFB_Locs = AFB_Locs.set_index('Base')




#Get dirty projection *** make this better later
Case_filter =  [col for col in AFB_Locs if col.endswith('days cases')]
def func(x, a, b, c):
    return a * b ** (x/c)
Proj_Params = []
for base in range(len(AFB_Locs)):
    Proj_Params.append(sp.optimize.curve_fit(func,xdata = np.array([*Case_Range])-np.min([*Case_Range]),ydata = AFB_Locs.loc[base,Case_filter], bounds=(0, 100))[0])
    for day_offset in range(0,5):
        AFB_Locs.loc[base,'Projected Cases '+ str(day_offset)] = func(-np.min([*Case_Range])+day_offset,Proj_Params[base][0],Proj_Params[base][1],Proj_Params[base][2])

# #These come in by date and State
# Proj_URL = 'https://ihmecovid19storage.blob.core.windows.net/latest/ihme-covid19.zip'
# response = requests.get(url=Proj_URL,headers=headers)
# response.close()
# proj_file_name = As_of_date[5:9] + "_0" + As_of_date[0:1]+ "_" + As_of_date[2:4]  + '/hospitalization_all_locs_corrected.csv'
# COVID_Projs = pd.read_csv(zipfile.ZipFile(io.BytesIO(response.content)).open(proj_file_name))
# response.close()
# COVID_Projs.State_x = COVID_Projs.location_name.map(us.states.mapping('name','abbr'))
#
# As_of_date_datetime =  datetime.datetime.strptime(As_of_date,'%m/%d/%Y').date()
# As_of_date_datetime + datetime.datetime.timedelta(days=1)
